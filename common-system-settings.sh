#!/bin/zsh

## macOS Preferences

# Add a context menu item for showing the Web Inspector in web views
defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

# Store screenshots in subfolder on desktop
mkdir ~/Screenshots
defaults write com.apple.screencapture location ~/Screenshots


## Github

# Test connection
ssh -T git@github.com

# Git global config
git config --global user.name "Gilles Demarty"
git config --global user.email "gilles@dmrty.fr"
git config --global github.user dmrty

git config --global core.editor "vi"
git config --global color.ui true

